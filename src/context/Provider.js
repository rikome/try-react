import React, { useState, useEffect } from 'react';
import axios from 'axios'

const DEFAULT_STATE = {}

export const ThemeContext = React.createContext(DEFAULT_STATE);

function Provider (props) {
  const [todos, setTodos] = useState([])
  const [task, setTask] = useState('')

  const reTask = (e) => setTask(e.target.value)

  const mark = (id) => {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          todo.completed = !todo.completed
        }
        return todo
      })
    )
  }

  const add = (e) => {
    e.preventDefault();
    console.log(task)
    todos.push({
      id: 23,
      title: task,
      completed: false
    })
  }

  const remove = (id) => {
    if (window.confirm(`Are you sure you want to remove todo ${id}`)) {
      axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then(res => {
        console.log(res.data)
        setTodos(
          todos.filter(todo => todo.id !== id)
        )
      })
    }
    console.log('delete ' + id)
  }

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/todos?_limit=8')
      .then(res => {
        setTodos(res.data)
      })
  }, [])

  useEffect(() => {
    console.log('I will run only when valueA changes');
  }, [task]);

  return (
    <ThemeContext.Provider
      value={{
        task,
        todos,
        mark,
        add,
        remove,
        reTask
      }}
    >
      {props.children}
    </ThemeContext.Provider>
  );
}

export default Provider