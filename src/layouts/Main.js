import React, { useState, useEffect } from 'react';
import Todos from '../components/Todos'
import axios from 'axios'

import bg from '../assets/images/pngtree-blue.jpg'

function Home () {
  const [todos, setTodos] = useState([])
  const [task, setTask] = useState('')

  const reTask = (e) => setTask(e.target.value)

  const mark = (id) => {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          todo.completed = !todo.completed
        }
        return todo
      })
    )
  }

  const add = (e) => {
    e.preventDefault();
    console.log(task)
    todos.push({
      id: 23,
      title: task,
      completed: false
    })
    // setTodos(yu)
  }

  const remove = (id) => {
    if (window.confirm(`Are you sure you want to remove todo ${id}`)) {
      axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then(res => {
        console.log(res.data)
        setTodos(
          todos.filter(todo => todo.id !== id)
        )
      })
    }
    console.log('delete ' + id)
  }

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/todos?_limit=8')
      .then(res => {
        setTodos(res.data)
      })
  }, [])

  const titleStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#fff',
    width: '100%',
    height: '200px',
    backgroundImage: `url(${bg})`,
    backgroundSize: 'cover'
  }

  return (
    <React.Fragment>
      <div style={titleStyle}>
        <h1>Home</h1>
      </div>
      <Todos list={todos} check={mark} delete={remove} save={add} entryText={task} updateEntry={reTask} />
    </React.Fragment>
  );
}

export default Home