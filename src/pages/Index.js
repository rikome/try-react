import React from 'react';
import Todos from '../components/Todos'

import bg from '../assets/images/pngtree-blue.jpg'

function Home () {
  const titleStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#fff',
    width: '100%',
    height: '200px',
    backgroundImage: `url(${bg})`,
    backgroundSize: 'cover'
  }

  return (
    <React.Fragment>
      <div style={titleStyle}>
        <h1>Home</h1>
      </div>
      <Todos />
    </React.Fragment>
  );
}

export default Home