import React from 'react';

function AddTodo (props) {
  return (    
    <form onSubmit={props.submit}>
      <p>Add New Task</p>
      <input type="text" value={props.text} onChange={props.textChange} />
    </form>
  );
}

export default AddTodo