import React from 'react';
import { Link } from 'react-router-dom'
import './Nav.css'

function Nav ({ links }) {
  return (
    <div style={{ display: 'flex' }}>
      {links.map(link => <Link key={link.id} to={link.to} className="navLink">{link.name}</Link>)}
    </div>
  );
}

export default Nav;