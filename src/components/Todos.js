import React, { useContext } from 'react';
import { ThemeContext } from '../context/Provider';

import MaterialIcon from 'material-icons-react';
import AddTodo from '../components/addTodo'

function Todos () {
  const afa = useContext(ThemeContext)
  console.log(afa)
  
  const itemList = {
    display: 'flex',
    flexDirection: 'row',
    padding: '5px 0px'
  }

  return (
    <ThemeContext.Consumer>
      {context => (
        <div style={{ width: '40%', border  : '1px solid #eee', padding: '15px' }} >
          <h6>Todos</h6>
          <ul style={{ fontSize: '15px', listStyle: 'none', padding: 0 }}>
            {context.todos.map(todo => <li key={todo.id} style={{ padding: '5px', listStyle: 'none' }}>
              <div style={itemList}>
                <input type="checkbox" checked={todo.completed ? true : false } onChange={context.mark.bind(this, todo.id)} />
                <div style={{ textDecoration: todo.completed ? 'line-through' : '', width: '100%' }}>
                  {todo.title}
                </div>  
                <div style={{ alignContent: 'flex-end', cursor: 'pointer' }}>
                  <MaterialIcon icon="cancel" color="red" size="15px" onClick={context.remove.bind(this, todo.id)} />
                </div>
              </div>
            </li>)}
          </ul>
          <AddTodo submit={context.add} text={context.task} textChange={context.reTask} />
        </div>
      )}
    </ThemeContext.Consumer>
  )
}

export default Todos