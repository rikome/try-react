import React from 'react'

function smallForm({ text, setG }) {
  return (
    <form>
      <input type="text" value={text} onChange={setG} />
      <h6 style={{ margin: 0 }}>{text}</h6>
      <button type="submit">Complete</button>
    </form>
  );
}

export default smallForm;