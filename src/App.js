import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Provider from './context/Provider'

import Nav from './components/Nav'
import Index from './pages/Index'
import About from './pages/About'
import Contact from './pages/Contact'

import './App.css';

function App() {
  const routes = [
    { id: 1, name: 'Home', to: '/', component: Index },
    { id: 2, name: 'About', to: '/about', component: About },
    { id: 3, name: 'Contact', to: '/contact', component: Contact }
  ]

  return (
    <Provider>
      <Router>
        <div className="App">
          <header className="App-header">
            <h5 style={{ margin: 0 }}>Test Project</h5>
            <Nav links={routes} />
          </header>
          <div className="App-body ">
            {routes.map(view => <Route key={view.id} exact={view.to === '/'} path={view.to} component={view.component} />)}
          </div>
          <footer className="App-footer">
            &copy; {new Date().getFullYear()} - Rikome
          </footer>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
